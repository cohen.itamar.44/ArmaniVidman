import numpy as np

def fit_polynomial(t_vals,thetas):
    return np.polyfit(t_vals, thetas, 3)

def predict_end(t_vals, thetas):
    """
    predict the stopping angle of the pendulum
    :param t_vals:
    :param thetas:
    :return: stop_time, stop_theta, fit_polynomial
    """
    fit_coefs = fit_polynomial(t_vals,thetas)
    root = find_first_root(min(t_vals), fit_coefs)
    derivative = np.polyder(fit_coefs)
    max_x = find_first_root(-0.5, derivative)
    end_t = 2 * root - max_x # flip around the root

    def pred_func(t):
        poly = np.poly1d(fit_coefs)
        res = np.real(poly(t))
        large_t = np.where(t > root)
        res[large_t] = -poly(2 * root - t[large_t])
        return np.real(res)

    return end_t, root, pred_func


def find_first_root(x_min, coefs):
    """
    find the first root of the polynomial after x_min
    :param x_min:
    :param coefs:
    :return:
    """
    roots = np.roots(coefs)
    real_roots = np.real(roots[np.imag(roots) == 0])
    return min(real_roots[(x_min <= real_roots)])
