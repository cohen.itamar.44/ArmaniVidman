import time

import matplotlib.pyplot as plt
import numpy as np
from scipy.optimize import least_squares
from prediction.math_utils import *
from prediction.solver_1d import predict_end


def _circle_eq(params, x):
    x_0, y_0 = params[:2]
    r = params[2]
    return (x[:, 0] - x_0) ** 2 + (x[:, 1] - y_0) ** 2 - r ** 2


def _ellipse_eq(params, x):
    h, k = params[:2]
    a = params[2]
    b = params[3]
    return ((x[:, 0] - h) * b) ** 2 + ((x[:, 1] - k) * a) ** 2 - (a * b) ** 2


def _plane_eq(params, x):
    a, b = params
    return a * x[:, 0] + b * x[:, 1] - 1


def fit_plane(samples_3d):
    """
    find the plane that is parallel to the z axis and best fits the data
    :param samples_3d: ndarray of shape (n,3)
    :return: (a,b) where the plane is ax+by-1=0
    """
    init = [1, 1]

    result = least_squares(_plane_eq, init, args=(samples_3d[:,:2],))
    return result.x[:2]


def fit_circle(samples_2d: np.ndarray):
    """
    :param samples_2d: ndarray of shape (n,2)
    :return: x_0, y_0, r
    """
    avg_x, diff_x = avg_and_diff(samples_2d[:, 0])
    avg_y, diff_y = avg_and_diff(samples_2d[:, 1])
    # set initial conditions
    init = np.zeros(3)
    init[0] = avg_x
    init[1] = avg_y
    init[2] = 100

    # min_radius = 100
    # s = 10  # will fail for very small release theta
    bounds = ([-np.inf, -np.inf, 80],
              [np.inf, np.inf, 300])

    # result = least_squares(_circle_eq, init, args=(samples_2d,), bounds=bounds)
    result = least_squares(_circle_eq, init, args=(samples_2d,),bounds=bounds)
    return result.x[:3]


def fit_ellipse(samples_2d):
    """
    fit an ellipse to a sample of 2d points
    :param samples_2d: np array of shape (n,2) with x,y coordiantes of samples
    :return: a list containing [x_center, y_center, x_radius, y_radius]
    """
    avg_x, diff_x = _avg_and_diff(samples_2d[:, 0])
    avg_y, diff_y = _avg_and_diff(samples_2d[:, 1])
    # set initial conditions
    init = np.zeros(4)
    init[0] = avg_x
    init[1] = avg_y
    init[2] = 2 * diff_x
    init[3] = 2 * diff_y
    min_radius = 100
    s = 10  # will fail for very small release theta
    bounds = ([avg_x - s * diff_x, avg_y - s * diff_y, 0, min_radius],
              [avg_x + s * diff_x, avg_y + s * diff_y, 2 * s * diff_x, 2 * s * diff_y])

    result = least_squares(_ellipse_eq, init, args=(samples_2d,), bounds=bounds)
    return result.x[:4]


def get_init(x, y):
    avg_x, diff_x = avg_and_diff(x)
    avg_y, diff_y = avg_and_diff(y)
    # set initial conditions
    init = np.zeros(4)
    init[0] = avg_x
    init[1] = avg_y
    init[2] = 2 * diff_x
    init[3] = 2 * diff_y
    return init


def solve_for_pivot_test():
    vid_path = r"..\recordings\red.MOV"
    crop = [0, 500, 100, 900]  # 2m_0deg
    # crop = [300, 500, 0, 480]  # 3.5m_0deg
    crop = [0, 650, 0, 1279]  # red
    end_t = 9
    measurements = detect_ball_from_path_old(vid_path, end_t, crop=crop, is_test=False)
    measurements[:, 1] *= -1
    first_measure = 100
    last_measure = 160
    t_0 = time.time()
    input_measurements = measurements[first_measure:last_measure, :]
    result = fit_ellipse(input_measurements)
    t_fit = time.time()
    center = result[:2]
    width = result[2]
    height = result[3]
    # print(f"x,y,width,height = {result[:4]}")

    coords = centralize(center, input_measurements)
    coords = stretch(width, coords, height)
    compare_to_ellipse(center, width, height, input_measurements)
    thetas = theta_from_xy(coords)
    t_convert_theta = time.time()
    camera_dt = 1 / 30
    t_vals = create_t_vals(camera_dt, input_measurements)

    end_t, root, poly_fit = predict_end(t_vals, thetas)
    t_end = time.time()
    print(f"time to fit ellipse: {t_fit - t_0}")
    print(f"time to get theta: {t_convert_theta - t_fit}")
    print(f"time to find prediction: {t_end - t_convert_theta}")
    print(f"total time: {t_end - t_0}")

    # this block plots the predicted end with
    non_train_theta = theta_from_xy(stretch(width, centralize(center, measurements[last_measure:]), height))
    non_train_t = create_t_vals(camera_dt, non_train_theta) + max(t_vals) + camera_dt
    plt.scatter(t_vals, thetas, label="training data")
    plt.scatter(non_train_t, non_train_theta, label="non train data")
    relevant_t = non_train_t[(0.6 <= non_train_t) & (1.4 >= non_train_t)]
    damp_factor = 0.9
    plt.scatter([end_t], [-poly_fit(2 * root - end_t) * damp_factor], label="end point")
    plt.plot(relevant_t, -poly_fit(2 * root - relevant_t) * damp_factor, label="predicted path")
    plt.legend()
    plt.show()


def create_t_vals(camera_dt, input_measurements):
    return np.arange(0, camera_dt * len(input_measurements), camera_dt)


def compare_to_ellipse(center, width, height, input_measurements, non_input_measurements=None):
    """
    compare an ellipse to the data that it was fitted to and the data that it was not fitted to
    :param center: (x,y)
    :param width:
    :param height:
    :param input_measurements: np array of shape (n,2) that was used for fit
    :param non_input_measurements: np array of shape (n,2) that was not used for fit
    """
    plt.scatter(input_measurements[:, 0], input_measurements[:, 1], label="in measurements")
    if non_input_measurements:
        plt.scatter(non_input_measurements[:, 0], non_input_measurements[:, 1], label="not in measurements")

    n_points = 500
    angles = np.linspace(0, 2 * np.pi, n_points)
    x = center[0] + width * np.cos(angles)
    y = center[1] + height * np.sin(angles)
    plt.plot(x, y, label="ellipse")

    plt.legend()
    plt.show()


def find_fit_and_theta(data_3d):
    a, b = fit_plane(data_3d)
    samples_2d = from_3d_to_2d_plane(data_3d, a, b)
    # find best fitting circle for points on the plane
    x_0, y_0, r = fit_circle(samples_2d)
    center = np.asarray((x_0, y_0))
    centralized_2d = centralize(center, samples_2d)
    # find theta of each point
    thetas = theta_from_xy(centralized_2d)
    return a, b, center, r, thetas


def theta_to_3d(a, b, center, r, thetas):
    xy = xy_from_rtheta(thetas, r) + center
    return from_2d_plane_to_3d(xy, a, b)

if __name__ == '__main__':
    solve_for_pivot_test()
