import numpy as np


def avg_and_diff(x):
    """
    return the avg and diff of the max and min vals in x
    :param x: np array
    :return:
    """
    max_x = np.max(x)
    min_x = np.min(x)
    return (max_x + min_x) / 2, max_x - min_x


def get_rotation_pivot(b) -> np.ndarray:
    """
    create a pivot for rotation of the plane, which is a point on the plane with x=0,z=0
    :param b:
    :return:
    """
    return np.asarray((0, 0, 0) if b == 0 else (0, 1 / b, 0))


def from_3d_to_2d_plane(samples_3d, a, b) -> np.ndarray:
    """
    project points in 3d onto the plane ax + by - 1 = 0
    :param samples_3d: points
    :param a:
    :param b:
    :return: the x,y coordinates of the points on the plane
    """
    pivot = get_rotation_pivot(b)
    diff = (samples_3d - pivot)[:, :2]

    projected = (-b * diff[:, 0] + a * diff[:, 1]) / np.sqrt(a ** 2 + b ** 2)

    result = np.zeros((samples_3d.shape[0], 2))
    result[:, 0] = projected
    result[:, 1] = samples_3d[:, 2]
    return result


def from_2d_plane_to_3d(samples_2d, a, b) -> np.ndarray:
    """
    inverse function of from_3d_to_2d_plane
    :param samples_2d:
    :param a:
    :param b:
    :return: points on the given plane, in 3d space
    """
    result = np.zeros((samples_2d.shape[0], 3))
    pivot = get_rotation_pivot(b)

    plane_vec = np.asarray([-b, a]) / np.sqrt(a ** 2 + b ** 2)
    result[:, 0] = plane_vec[0] * samples_2d[:, 0]
    result[:, 1] = plane_vec[1] * samples_2d[:, 0]
    result[:, 2] = samples_2d[:, 1]
    result += pivot
    return result


def centralize(center, coordinates_2d) -> np.ndarray:
    """
    move the center of the ellipse to the origin
    """
    return coordinates_2d - center


def stretch(width, coordinates_2d, height) -> np.ndarray:
    """
    stretch the minor axis (width) of the ellipse to be in the same scale of the major axis
    """
    res = coordinates_2d
    res[:, 0] *= height / width
    return res


def theta_from_xy(coordinates_2d) -> np.ndarray:
    """
    assumes points are on a canonical circle, returns theta as defined in a pendulum
    :param coordinates_2d: x,y of points
    :return: 1d np array of theta
    """
    x = coordinates_2d[:, 0]
    y = coordinates_2d[:, 1]
    return np.arcsin(x / np.sqrt((x ** 2 + y ** 2)))


def xy_from_rtheta(theta, r) -> np.ndarray:
    """
    get x y from r and theta as defined in a pendulum
    :param theta:
    :param r: radius
    :return:
    """
    res = np.zeros((theta.shape[0], 2))
    res[:, 0] = r * np.sin(theta)
    res[:, 1] = -r * np.cos(theta)
    return res


def xy_rotation_matrix(theta) -> np.ndarray:
    """
    create a 3d rotation matrix that rotates by theta around z axis
    :param theta:
    :return:
    """
    return np.array([[np.cos(theta), -np.sin(theta), 0],
                     [np.sin(theta), np.cos(theta), 0],
                     [0, 0, 1]])


def running_average(arr, window_size):
    # Pad the array to handle edges
    running_avg = np.convolve(arr, np.ones(window_size) / window_size, mode='same')
    return running_avg
