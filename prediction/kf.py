import numpy as np
from filterpy.kalman import KalmanFilter
from matplotlib import pyplot as plt


def create_2d_kf(dt, init_conditions):
    # Define the Kalman Filter
    kf = KalmanFilter(dim_x=6, dim_z=2)
    kf.F = np.array([[1, 0, dt, 0, 0.5 * dt ** 2, 0],
                     [0, 1, 0, dt, 0, 0.5 * dt ** 2],
                     [0, 0, 1, 0, dt, 0],
                     [0, 0, 0, 1, 0, dt],
                     [0, 0, 0, 0, 1, 0],
                     [0, 0, 0, 0, 0, 1]])
    kf.H = np.array([[1, 0, 0, 0, 0, 0],
                     [0, 1, 0, 0, 0, 0]])

    # Define process noise covariance matrix
    process_noise = 0.3  # Process noise covariance
    kf.Q = np.eye(6) * process_noise
    # kf.Q = np.ones((6,6)) * process_noise
    # Define measurement noise covariance matrix
    measurement_noise = 0.05  # Measurement noise covariance
    kf.R = np.eye(2) * measurement_noise
    kf.P = np.eye(6)  # Initial covariance matrix
    kf.x = init_conditions
    return kf


def kf_test():
    sim_dt = 0.001
    camera_dt = 1 / 10
    from prediction.simulation import sample_from_simulated_data, simulate
    sim_res = simulate(t_end=1, init_conditions=np.array([np.pi / 2.5, 0]))
    data = sample_from_simulated_data(sim_dt, camera_dt, sim_res)
    data = data[:, 1:]
    # data = np.zeros((100,2))
    # data[:,0] = np.arange(0,100)**2
    kf = create_2d_kf(camera_dt, init_conditions=np.concatenate([data[0], np.zeros(4)]))
    real_data = data[1:, :]
    measurements = real_data + np.random.normal(0, 0.05, real_data.shape)
    filtered_states = []

    for z in measurements:
        kf.predict()
        filtered_states.append(kf.x)
        kf.update(z)

    predicted = np.asarray(filtered_states)[:, :2]
    plt.scatter(real_data[:, 0], real_data[:, 1], label="real data")
    plt.scatter(measurements[:, 0], measurements[:, 1], label="measurement")
    plt.scatter(predicted[:, 0], predicted[:, 1], label="prediction")
    plt.legend()
    plt.show()
    print(real_data.size, measurements.size, predicted.size)


"""
# Perform Kalman filtering
filtered_states = []
for z in measurements:
    kf.predict()
    kf.update(z)
    filtered_states.append(kf.x)
filtered_states = np.array(filtered_states)

# Print the filtered states
for state in filtered_states:
    print(state)
plt.plot(range(len(filtered_states)), filtered_states[:, 0])
plt.plot(range(len(filtered_states)), true_states[:, 1])
plt.show()
"""
