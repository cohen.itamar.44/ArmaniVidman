import json
from typing import Any

import numpy as np
from calibration.calibration_window import CalibrationWindow
from calibration.nonlinear_calibration_window import NonlinearCalibrationWindow

CALIB_DATA_FILENAME = 'nonlinear_calib_results.json'

def save_json(filename, pts, base_values, pitch_values, b_range, p_range, shape, ij: Any = ''):
    data_arrays_dict = {
        'pts': pts, 'base_values': base_values, 'pitch_values': pitch_values,
        'b_range': b_range, 'p_range': p_range, 'shape': shape, 'ij': ij
    }
    with open(filename, 'w') as json_file:
        json.dump(data_arrays_dict, json_file)


def run_nonlinear_calibration(calibrator: NonlinearCalibrationWindow):
    start_new = input('start a new calibration? (y/n)') == 'y'

    ij = None
    skipping = False

    if start_new:
        b_min, b_max, p_min, p_max = 1.5, 12, 17.5, 32.5
        b_origin_val, p_origin_val = (b_min + b_max) / 2, (p_min + p_max) / 2
        b_range_rad, p_range_rad = (b_max - b_min) / 2, (p_max - p_min) / 2
        shapearr = input('insert shape size (b p): ').split()
        shape = (int(shapearr[0]), int(shapearr[1]))

        points = list()
        base_angle_values = list()
        pitch_angle_values = list()
    else:
        with open(CALIB_DATA_FILENAME, 'r') as calib_file:
            calib_data = json.load(calib_file)

        b_min, b_max = calib_data['b_range']
        p_min, p_max = calib_data['p_range']
        b_origin_val, p_origin_val = (b_min + b_max) / 2, (p_min + p_max) / 2
        b_range_rad, p_range_rad = (b_max - b_min) / 2, (p_max - p_min) / 2
        shape = calib_data['shape']
        points = calib_data['pts']
        base_angle_values = calib_data['base_values']
        pitch_angle_values = calib_data['pitch_values']
        if calib_data['ij'] != '':
            ij = [int(index) for index in calib_data['ij']]
            skipping = True

    calibrator.set_reset_vals(b_origin_val, p_origin_val)
    calibrator.mouse_event.clear()
    for i, b_diff in enumerate(np.linspace(-b_range_rad, b_range_rad, shape[0])):
        for j, p_diff in enumerate(np.linspace(-p_range_rad, p_range_rad, shape[1])):

            if skipping:
                if (i, j) == ij:
                    skipping = False
                continue

            calibrator.reset()

            calibrator.reset_event.wait()

            calibrator.set_values(b_diff, p_diff)

            # wait until Button is pressed
            calibrator.mouse_event.wait()
            calibrator.mouse_event.clear()

            # record sample
            if calibrator.mouse_norm is not None:
                points.append(calibrator.mouse_norm)
                base_angle_values.append(b_diff)
                pitch_angle_values.append(p_diff)
            save_json(CALIB_DATA_FILENAME, points, base_angle_values, pitch_angle_values,
                      (b_min, b_max), (p_min, p_max), shape, ij=(i, j))

    print('Done')

    save_json(CALIB_DATA_FILENAME, points, base_angle_values, pitch_angle_values,
              (b_min, b_max), (p_min, p_max), shape)
