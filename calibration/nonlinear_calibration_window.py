import tkinter as tk
import cv2
import numpy as np
from PIL import Image, ImageTk
import threading

from launch.arduino_controller import ArduinoController
from launch.nonlinear_calib_eng import NonlinearCalibrationEngine

STEP_X, STEP_Y = -1, 1
CAMERA_INDEX = 1

class NonlinearCalibrationWindow:

    def __init__(self, root, ardu_controller: ArduinoController, calib_eng: NonlinearCalibrationEngine):
        self.root = root
        self.root.title("Camera Viewer and Control")
        self.root.geometry("800x600")

        self.ardu_controller = ardu_controller
        self.calib_eng = calib_eng

        self.x = 0
        self.y = 0

        self.reset_vals = None
        self.curr_origin = None

        self.next_values = (0, 0)

        self.width, self.height = 0, 0

        # Create a label to display the camera image
        self.label = tk.Label(self.root)
        self.label.pack(padx=10, pady=10)

        # Bind arrow key presses to update_position function
        self.root.bind('<KeyPress>', self.update_position)
        self.root.bind('<space>', self.set_origin)
        self.label.bind('<Button-1>', self.on_mouse_click)
        self.label.bind('<Button-2>', self.fire)
        self.label.bind('<Button-3>', self.dismiss_sample)

        self.reset_event = threading.Event()
        self.mouse_event = threading.Event()

        # Start reading camera feed in a separate thread
        self.camera_thread = threading.Thread(target=self.read_camera)
        self.camera_thread.daemon = True
        self.camera_thread.start()

    def update_position(self, event):
        if event.keysym not in ['Up', 'Down', 'Left', 'Right', 'w', 's', 'a', 'd', 'r']:
            return

        # change self.x and self.y
        if event.keysym in ['Up', 'w']:
            self.y += STEP_Y * (1 if event.keysym == 'Up' else 0.1)
        elif event.keysym in ['Down', 's']:
            self.y -= STEP_Y * (1 if event.keysym == 'Down' else 0.1)
        elif event.keysym in ['Left', 'a']:
            self.x -= STEP_X * (1 if event.keysym == 'Left' else 0.1)
        elif event.keysym in ['Right', 'd']:
            self.x += STEP_X * (1 if event.keysym == 'Right' else 0.1)
        elif event.keysym in ['r']:
            self.x = 0
            self.y = 0
            print("going to (0,0)")
        self.x, self.y = round(self.x, 1), round(self.y, 1)

        # send command to arduino
        self.ardu_controller.set_base_angle(self.x)
        self.ardu_controller.set_pitch_angle(self.y)

    def on_mouse_click(self, event):
        self.mouse_norm = (event.x / self.width, event.y / self.height)
        if self.calib_eng:
            b_diff, p_diff = self.calib_eng.ij_to_base_pitch_diffs(*self.mouse_norm)
            self.x = self.curr_origin[0] + b_diff
            self.y = self.curr_origin[1] + p_diff
            self.ardu_controller.set_base_angle(self.x)
            self.ardu_controller.set_pitch_angle(self.y)
        else:
            self.mouse_event.set()

    def dismiss_sample(self, event):
        self.mouse_norm = None
        self.mouse_event.set()

    def read_camera(self):
        cap = cv2.VideoCapture(CAMERA_INDEX)  # Open the default camera (change index if you have multiple cameras)
        if not cap.isOpened():
            print("Error: Could not open camera.")
            return

        while cap.isOpened():
            ret, frame = cap.read()
            if not ret:
                print("Error: Failed to capture frame.")
                break

            self.height, self.width = frame.shape[:2]
            # Convert frame from BGR to RGB and then to ImageTk format

            frame_rgb = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
            img = Image.fromarray(frame_rgb)
            img_tk = ImageTk.PhotoImage(image=img)

            # Update GUI with the new image
            self.label.config(image=img_tk)
            self.label.image = img_tk

            # Check for arrow key press events
            key = cv2.waitKey(1) & 0xFF
            if key == 27:  # ESC key to exit
                break

        cap.release()
        cv2.destroyAllWindows()

    def set_reset_vals(self, b_origin_val, p_origin_val):
        self.reset_vals = (b_origin_val, p_origin_val)

    def reset(self):
        print(f'reseting to reset vals - press space when on origin')
        self.x, self.y = (0, 0) if self.reset_vals is None else self.reset_vals
        self.ardu_controller.set_base_angle(self.x)
        self.ardu_controller.set_pitch_angle(self.y)
        self.reset_event.clear()

    def set_origin(self, event):
        self.curr_origin = (self.x, self.y)
        self.reset_vals = (self.x, self.y)
        self.reset_event.set()

    def set_values(self, b_diff, p_diff):
        self.x = self.curr_origin[0] + b_diff
        self.y = self.curr_origin[1] + p_diff
        self.ardu_controller.set_base_angle(self.x)
        self.ardu_controller.set_pitch_angle(self.y)

    def fire(self, event):
        self.ardu_controller.load_gun()
        self.ardu_controller.shoot()


if __name__ == "__main__":
    root = tk.Tk()
    ardu_controller = ArduinoController(port='COM10')
    calib_eng = NonlinearCalibrationEngine()
    calibrator = NonlinearCalibrationWindow(root, ardu_controller, calib_eng)

    from calibration.nonlinear_calibration_procedure import run_nonlinear_calibration

    calib_thread = threading.Thread(
        target=lambda: run_nonlinear_calibration(calibrator)
    )
    calib_thread.start()

    root.mainloop()
