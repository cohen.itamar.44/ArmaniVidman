import tkinter as tk
import cv2
import numpy as np
from PIL import Image, ImageTk
import threading

from launch.arduino_controller import ArduinoController
from launch.calibration_engine import CalibrationEngine

STEP_X, STEP_Y = -1, 1
CAMERA_INDEX = 1

class CalibrationWindow:

    def __init__(self, root, ardu_controller: ArduinoController, calib_eng: CalibrationEngine):
        self.root = root
        self.root.title("Camera Viewer and Control")
        self.root.geometry("800x600")

        self.ardu_controller = ardu_controller
        self.calib_eng = calib_eng

        self.x = 0
        self.y = 0

        self.width, self.height = 0, 0

        # Create a label to display the camera image
        self.label = tk.Label(self.root)
        self.label.pack(padx=10, pady=10)

        # Create a label to display current position
        self.depth_label = tk.Label(self.root, text=f'')
        self.depth_label.pack(pady=3)
        self.angle_label = tk.Label(self.root, text=f"Angles: --")
        self.angle_label.pack(pady=3)
        self.value_label = tk.Label(self.root, text=f"Values: --")
        self.value_label.pack(pady=3)

        # Bind arrow key presses to update_position function
        self.root.bind('<KeyPress>', self.update_position)
        self.label.bind("<Button-1>", self.on_mouse_click)
        self.label.bind("<Button-3>", self.shoot)

        self.button_event = threading.Event()

        def activate_event():
            self.button_event.set()
        self.sample_button = tk.Button(self.root, text="Sample Values", command=activate_event)
        self.sample_button.pack(pady=3)

        # Start reading camera feed in a separate thread
        self.camera_thread = threading.Thread(target=self.read_camera)
        self.camera_thread.daemon = True
        self.camera_thread.start()

        self._target_pos_normalized = None
        self._lock = threading.Lock()

    def update_position(self, event):
        if event.keysym not in ['Up', 'Down', 'Left', 'Right', 'w', 's', 'a', 'd', 'r']:
            return

        # change self.x and self.y
        if event.keysym in ['Up', 'w']:
            self.y += STEP_Y * (1 if event.keysym == 'Up' else 0.1)
        elif event.keysym in ['Down', 's']:
            self.y -= STEP_Y * (1 if event.keysym == 'Down' else 0.1)
        elif event.keysym in ['Left', 'a']:
            self.x -= STEP_X * (1 if event.keysym == 'Left' else 0.1)
        elif event.keysym in ['Right', 'd']:
            self.x += STEP_X * (1 if event.keysym == 'Right' else 0.1)
        self.x, self.y = round(self.x, 1), round(self.y, 1)

        # update labels
        self.angle_label.config(text=f"Angles: base={self.x}, pitch={self.y}")
        b_val = self.ardu_controller.convert_base_angle_to_value(self.x)
        p_val = self.ardu_controller.convert_pitch_angle_to_value(self.y)
        self.value_label.config(text=f"Angles: base={b_val}, pitch={p_val}")

        # send command to arduino
        self.ardu_controller.set_base_angle(self.x)
        self.ardu_controller.set_pitch_angle(self.y)

    def on_mouse_click(self, event):
        if self.calib_eng is not None:
            try:
                self.x, self.y = self.calib_eng.ijz_to_base_pitch_angles(np.array([event.x / self.width, event.y / self.height, 0]))
                self.ardu_controller.set_base_angle(self.x)
                self.ardu_controller.set_pitch_angle(self.y)
            except ValueError as e:
                print(f'x={event.x / self.width}, y={event.y / self.height}')

    def shoot(self, event):
        print('pew')
        self.ardu_controller.load_gun()
        self.ardu_controller.shoot()

    def update_target_z(self, z):
        self.depth_label.config(text=f'Target is at depth - f{z}')

    def read_camera(self):
        cap = cv2.VideoCapture(CAMERA_INDEX)  # Open the default camera (change index if you have multiple cameras)
        if not cap.isOpened():
            print("Error: Could not open camera.")
            return

        while cap.isOpened():
            ret, frame = cap.read()
            if not ret:
                print("Error: Failed to capture frame.")
                break

            self.height, self.width = frame.shape[:2]
            # Convert frame from BGR to RGB and then to ImageTk format

            if self.target_pos_normalized:
                x_norm, y_norm = self.target_pos_normalized
                target_pos = (int(x_norm * self.width), int(y_norm * self.height))
                # draws a circle with radius=4, color=RED, thickness=2
                cv2.circle(frame, target_pos, 4, (0, 0, 255), 2)

            frame_rgb = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
            img = Image.fromarray(frame_rgb)
            img_tk = ImageTk.PhotoImage(image=img)

            # Update GUI with the new image
            self.label.config(image=img_tk)
            self.label.image = img_tk

            # Check for arrow key press events
            key = cv2.waitKey(1) & 0xFF
            if key == 27:  # ESC key to exit
                break

        cap.release()
        cv2.destroyAllWindows()

    @property
    def target_pos_normalized(self):
        with self._lock:
            return self._target_pos_normalized

    @target_pos_normalized.setter
    def target_pos_normalized(self, value):
        with self._lock:
            self._target_pos_normalized = value



if __name__ == "__main__":
    root = tk.Tk()
    ardu_controller = ArduinoController(port='COM10')
    # calib_eng = CalibrationEngine()
    calibrator = CalibrationWindow(root, ardu_controller, None)

    from calibration.calibration_procedure import run_calibration
    calib_thread = threading.Thread(target=lambda: run_calibration(calibrator))
    calib_thread.start()

    root.mainloop()
