import numpy as np

from launch.arduino_controller import ArduinoController
from launch.calibration_engine import CalibrationEngine

import tkinter as tk
from tkinter import Label
import cv2
from PIL import Image, ImageTk

VIDEO_SOURCE = 1


class CalibTest:
    def __init__(self, window, click_callback, video_source=VIDEO_SOURCE):
        self.window = window
        self.window.title('Calibration Testing')

        self.vid = cv2.VideoCapture(video_source)
        if not self.vid.isOpened():
            raise ValueError("Unable to open video source", video_source)

        self.canvas = tk.Canvas(window, width=self.vid.get(cv2.CAP_PROP_FRAME_WIDTH),
                                height=self.vid.get(cv2.CAP_PROP_FRAME_HEIGHT))
        self.canvas.pack()

        self.click_callback = click_callback
        self.canvas.bind("<Button-1>", self.mouse_click)

        self.delay = 15
        self.update()

        self.window.mainloop()

    def update(self):
        ret, frame = self.vid.read()
        if ret:
            self.photo = ImageTk.PhotoImage(image=Image.fromarray(cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)))
            self.canvas.create_image(0, 0, image=self.photo, anchor=tk.NW)
        self.window.after(self.delay, self.update)

    def mouse_click(self, event):
        x, y = event.x, event.y
        canvas_width = self.canvas.winfo_width()
        canvas_height = self.canvas.winfo_height()
        x_relative = x / canvas_width
        y_relative = y / canvas_height
        self.click_callback(x_relative, y_relative)

    def __del__(self):
        if self.vid.isOpened():
            self.vid.release()


if __name__ == '__main__':
    root = tk.Tk()

    calib_engine = CalibrationEngine()
    ardu_cnt = ArduinoController(port='COM10')


    def mouse_clicked(x_rel, y_rel):
        base_angle, pitch_angle = calib_engine.ijz_to_base_pitch_angles(np.array([x_rel, y_rel, 0]))
        print('sending')
        ardu_cnt.set_base_angle(base_angle)
        ardu_cnt.set_pitch_angle(pitch_angle)


    app = CalibTest(root, mouse_clicked)
    app.bind_mouse_click(mouse_clicked())

    root.mainloop()
