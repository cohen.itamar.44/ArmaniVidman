import threading
import time

import cv2
import tkinter as tk

import numpy as np
from PIL import Image, ImageTk

CAM1 = 1
CAM2 = 2

BASELINE = 30

CAMERAS = {
    "dor_cam": {
        "name": "Dor Camera",
        "f_x": 519.39266469,
        "f_y": 518.33370266,
        "o_x": 321.4443738,
        "o_y": 243.68356671,
    },
    "logi_cam": {
        "name": "Logi Camera",
        "f_x": 626.0716671,
        "f_y": 622.99841535,
        "o_x": 297.73690509,
        "o_y": 249.3490038,
    },
    "comp_cam": {
        "name": "Other Camera",
        "f_x": 635.67829423,
        "f_y": 629.56080874,
        "o_x": 318.39348735,
        "o_y": 241.18257172,
    },
    "new_cam": {
        "name": "new_cam",
        "f_x": 565.46,
        "f_y": 565.52,
        "o_x": 315.64,
        "o_y": 216.65,
    }
}
# from calibration with 20 imgs
#[[575.09734466   0.         301.60805445]
# [  0.         573.76708463 208.4198637 ]
# [  0.           0.           1.        ]]

class TestWindow:
    def __init__(self, root, callback, cam_name_1, cam_name_2):
        self.root = root
        self.root.title("Camera Viewer and Control")
        self.root.geometry("800x600")

        self.callback = callback

        self.point1, self.point2 = None, None
        self.cam_name_1, self.cam_name_2 = cam_name_1, cam_name_2

        # Create a frame to hold the labels
        self.frame = tk.Frame(self.root)
        self.frame.pack(padx=10, pady=10)

        # Create the first label and pack it to the left side of the frame
        self.label1 = tk.Label(self.frame, text="Waiting for Camera")
        self.label1.pack(side=tk.LEFT, padx=10, pady=10)

        # Create the second label and pack it to the left side of the frame
        self.label2 = tk.Label(self.frame, text="Waiting for Camera")
        self.label2.pack(side=tk.LEFT, padx=10, pady=10)

        self.res_label = tk.Label(self.root)
        self.res_label.pack(padx=20, pady=20)

        self.filming = True

        self.label1.bind("<Button-1>", self.on_mouse_click1)
        self.label2.bind("<Button-1>", self.on_mouse_click2)

        def on_space(arg):
            self.point1, self.point2 = None, None
            self.filming = not self.filming
        root.bind("<space>", on_space)

        self.camera_thread = threading.Thread(target=self.read_cameras)
        self.camera_thread.daemon = True
        self.camera_thread.start()

    def on_mouse_click1(self, event):
        # Save the coordinates of the mouse click
        self.point1 = (event.x, event.y)
        if self.point2 is not None:
            self.res_label.config(text=self.callback(self.point1, self.point2, self.cam_name_1, self.cam_name_2))

    def on_mouse_click2(self, event):
        # Save the coordinates of the mouse click
        self.point2 = (event.x, event.y)
        if self.point1 is not None:
            self.res_label.config(text=self.callback(self.point1, self.point2, self.cam_name_1, self.cam_name_2))

    def read_cameras(self):
        cap1, cap2 = cv2.VideoCapture(CAM1), cv2.VideoCapture(CAM2)
        if not cap1.isOpened() or not cap2.isOpened():
            print("Error: Could not open camera.")
            return

        frame1, frame2 = None, None
        while cap1.isOpened() and cap2.isOpened():
            if self.filming:
                ret1, frame1 = cap1.read()
                ret2, frame2 = cap2.read()
                if not ret1 or not ret2:
                    print("Error: Failed to capture frame.")
                    break

            f1, f2 = frame1.copy(), frame2.copy()
            if self.point1 is not None:
                cv2.circle(f1, self.point1, 3, (0, 0, 255), -1)
            if self.point2 is not None:
                cv2.circle(f2, self.point2, 3, (255, 0, 0), -1)

            # Convert frame from BGR to RGB and then to ImageTk format
            frame1_rgb = cv2.cvtColor(f1, cv2.COLOR_BGR2RGB)
            img1 = Image.fromarray(frame1_rgb)
            img1_tk = ImageTk.PhotoImage(image=img1)
            frame2_rgb = cv2.cvtColor(f2, cv2.COLOR_BGR2RGB)
            img2 = Image.fromarray(frame2_rgb)
            img2_tk = ImageTk.PhotoImage(image=img2)

            # Update GUI with the new image
            self.label1.config(image=img1_tk)
            self.label1.image = img1_tk
            self.label2.config(image=img2_tk)
            self.label2.image = img2_tk

            # Check for arrow key press events
            key = cv2.waitKey(1) & 0xFF
            if key == 27:  # ESC key to exit
                break

        cap1.release()
        cap2.release()
        cv2.destroyAllWindows()


def screen_point_to_xy(point):
    print(point)
    f_x = 635.67829423
    f_y = 629.56080874
    o_x = 318.39348735
    o_y = 241.18257172
    z = 200

    x = z * (point[0] - o_x) / f_x
    y = z * (point[1] - o_y) / f_y

    print(f"X: {x}, Y: {y}, Z: {z}")


def screen_point_to_xy_dor_camera(point):
    print(point)
    f_x = 519.39266469
    f_y = 518.33370266
    o_x = 321.4443738
    o_y = 243.68356671
    z = 200




    x = z * (point[0] - o_x) / f_x
    y = z * (point[1] - o_y) / f_y

    print(f"X: {x}, Y: {y}, Z: {z}")


def screen_point_to_xy_logi_camera(point):
    print(point)
    f_x = 626.0716671
    f_y = 622.99841535
    o_x = 297.73690509
    o_y = 249.3490038
    z = 170

    x = z * (point[0] - o_x) / f_x
    y = z * (point[1] - o_y) / f_y

    print(f"X: {x}, Y: {y}, Z: {z}")

def screen_point_to_xy_general(point, camera_stats):
    f_x = camera_stats['f_x']
    f_y = camera_stats['f_y']
    o_x = camera_stats['o_x']
    o_y = camera_stats['o_y']

    x_over_y = (point[0] - o_x) / f_x
    z_over_y = -(point[1] - o_y) / f_y

    return x_over_y, z_over_y


def xyz_to_screen_point(xyz, camera_stats):
    f_x = camera_stats['f_x']
    f_y = camera_stats['f_y']
    o_x = camera_stats['o_x']
    o_y = camera_stats['o_y']

    x, y, z = xyz
    i = (x/y) * f_x + o_x
    j = (-z/y) * f_y + o_y

    return i, j


def get_depth(x_over_z_cam_1, x_over_z_cam_2, distance_between_cams):
    try:
        depth = distance_between_cams / (x_over_z_cam_1 - x_over_z_cam_2)
    except Exception as e:
        return 0

    return depth


def screen_points_to_depth(point1, point2, cam_name_1, cam_name_2):
    x_over_y_1, _ = screen_point_to_xy_general(point1, CAMERAS[cam_name_1])
    x_over_y_2, _ = screen_point_to_xy_general(point2, CAMERAS[cam_name_2])
    depth = get_depth(x_over_y_1, x_over_y_2, BASELINE)
    return depth


def screen_point_depth_to_xyz(point, depth, cam_name):
    x_over_y, z_over_y = screen_point_to_xy_general(point, CAMERAS[cam_name])
    return np.array([x_over_y, 1, z_over_y]) * depth


if __name__ == "__main__":
    root = tk.Tk()
    TestWindow(root, screen_points_to_depth, "new_cam", "new_cam")

    root.mainloop()

