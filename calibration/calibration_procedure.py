import json
from typing import Any

import numpy as np
from calibration.calibration_window import CalibrationWindow

CALIB_DATA_FILENAME = 'calib_results.json'


def save_json(filename, shape, z_len, z_range, offset_arr, base_angle_lookup, pitch_angle_lookup, ijk: Any = ''):
    data_arrays_dict = {
        'x_pt': shape[0], 'y_pt': shape[1], 'z_pt': z_len, 'z_min': z_range[0], 'z_max': z_range[1],
        'offset': offset_arr, 'ijk': ijk,
        'base_angles': base_angle_lookup.tolist(),
        'pitch_angles': pitch_angle_lookup.tolist()
    }

    with open(filename, 'w') as json_file:
        json.dump(data_arrays_dict, json_file)


def run_calibration(calibrator: CalibrationWindow):
    start_new = input('start a new calibration? (y/n)') == 'y'

    ijk = None
    skipping = False

    if start_new:
        inarr = input("insert the shape of the lookup table (x y z): ").split()
        shape = (int(inarr[0]), int(inarr[1]))
        z_len = int(inarr[2])
        z_range = [float(x) for x in input('insert the range of depth (z1 z2): ').split()]
        offset = input(
            "insert the offset percent of the lookup table (x_0 x_1 y_0 y_1): ").split()  # TODO: also insert z range
        offset_arr = ((float(offset[0]), float(offset[1])), (float(offset[2]), float(offset[3])))
        base_angle_lookup = np.zeros([*shape, z_len])
        pitch_angle_lookup = np.zeros([*shape, z_len])
    else:
        with open(CALIB_DATA_FILENAME, 'r') as calib_file:
            calib_data = json.load(calib_file)
        shape = (calib_data['x_pt'], calib_data['y_pt'])
        z_len = calib_data['z_pt']
        z_range = (calib_data['z_min'], calib_data['z_max'])
        offset_arr = calib_data['offset']
        base_angle_lookup = np.array(calib_data['base_angles'])
        pitch_angle_lookup = np.array(calib_data['pitch_angles'])
        if calib_data['ijk'] != '':
            ijk = [int(index) for index in calib_data['ijk']]
            skipping = True

    calibrator.button_event.clear()
    for k, z in enumerate(np.linspace(*z_range, z_len)):
        calibrator.update_target_z(z)
        for index in np.ndindex(*shape):

            if skipping:
                if [index[0], index[1], k] == ijk:
                    skipping = False
                continue

            # set target on window, i_norm and j_norm are normalized screen coordinates (0-1)
            offset_x = offset_arr[0]
            offset_y = offset_arr[1]

            i_norm, j_norm = offset_x[0] + index[0] * (offset_x[1] - offset_x[0]) / (shape[0] - 1), offset_y[0] + index[
                1] * (offset_y[1] - offset_y[0]) / (shape[1] - 1)
            calibrator.target_pos_normalized = (i_norm, j_norm)

            # wait until Button is pressed
            calibrator.button_event.wait()
            # record sample

            base_angle_lookup[index[0], index[1], k] = calibrator.x
            pitch_angle_lookup[index[0], index[1], k] = calibrator.y
            save_json(shape, z_len, z_range, offset_arr, base_angle_lookup, pitch_angle_lookup,
                      ijk=[index[0], index[1], k])

            calibrator.button_event.clear()

    calibrator.target_pos_normalized = None
    print('Done')

    save_json(shape, z_len, z_range, offset_arr, base_angle_lookup, pitch_angle_lookup)
