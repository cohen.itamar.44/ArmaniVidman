import threading

import cv2
import tkinter as tk

from PIL import Image, ImageTk

CAM1 = 1

CAMERAS = {
    "Dor Camera": {
        "name": "Dor Camera",
        "f_x": 519.39266469,
        "f_y": 518.33370266,
        "o_x": 321.4443738,
        "o_y": 243.68356671,
    },
    "Logi Camera": {
        "name": "Logi Camera",
        "f_x": 626.0716671,
        "f_y": 622.99841535,
        "o_x": 297.73690509,
        "o_y": 249.3490038,
    },
    "Computer Camera": {
        "name": "Other Camera",
        "f_x": 635.67829423,
        "f_y": 629.56080874,
        "o_x": 318.39348735,
        "o_y": 241.18257172,
    }
}


class CalibrationWindow:
    def __init__(self, root, callback):
        self.root = root
        self.root.title("Camera Viewer and Control")
        self.root.geometry("800x600")
        self.camera_one_name = "Dor Camera"
        self.callback = callback

        self.point = None

        # Create a label to display the camera image
        self.label = tk.Label(self.root)
        self.label.pack(padx=10, pady=10)

        self.label.bind("<Button-1>", self.on_mouse_click)

        self.camera_thread = threading.Thread(target=self.read_camera)
        self.camera_thread.daemon = True
        self.camera_thread.start()

    def on_mouse_click(self, event):
        # Save the coordinates of the mouse click
        self.point = (event.x, event.y)
        self.callback(self.point, CAMERAS[self.camera_one_name])

    def read_camera(self):
        cap = cv2.VideoCapture(CAM1)  # Open the default camera (change index if you have multiple cameras)
        if not cap.isOpened():
            print("Error: Could not open camera.")
            return

        while cap.isOpened():
            ret, frame = cap.read()
            center_x, center_y = frame.shape[1] // 2, frame.shape[0] // 2
            cv2.circle(frame, (center_x, center_y), 3, (255, 0, 0), -1)
            if not ret:
                print("Error: Failed to capture frame.")
                break

            if self.point is not None:
                cv2.circle(frame, self.point, 3, (0, 255, 0), -1)

            # Convert frame from BGR to RGB and then to ImageTk format
            frame_rgb = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
            img = Image.fromarray(frame_rgb)
            img_tk = ImageTk.PhotoImage(image=img)

            # Update GUI with the new image
            self.label.config(image=img_tk)
            self.label.image = img_tk

            # Check for arrow key press events
            key = cv2.waitKey(1) & 0xFF
            if key == 27:  # ESC key to exit
                break

        cap.release()
        cv2.destroyAllWindows()


def screen_point_to_xy_general(point, camera_stats):
    print(point)
    f_x = camera_stats['f_x']
    f_y = camera_stats['f_y']
    o_x = camera_stats['o_x']
    o_y = camera_stats['o_y']
    # z = 200

    x_over_z = (point[0] - o_x) / f_x
    y_over_z = (point[1] - o_y) / f_y

    print(f"X: {x_over_z}, Y: {y_over_z}")

    return x_over_z, y_over_z


def get_depth(x_over_z_cam_1, distance_between_cams, point, main_camera_stats):
    f_x = main_camera_stats["f_x"]
    f_y = main_camera_stats["f_y"]
    o_x = main_camera_stats["o_x"]
    o_y = main_camera_stats["o_y"]
    depth = distance_between_cams / (x_over_z_cam_1 - (point[0]-o_x) / f_x)

    return depth


def screen_point_to_xy_my_computer(point):
    print(point)
    f_x = 635.67829423
    f_y = 629.56080874
    o_x = 318.39348735
    o_y = 241.18257172
    z = 200

    x = z * (point[0] - o_x) / f_x
    y = z * (point[1] - o_y) / f_y

    print(f"X: {x}, Y: {y}, Z: {z}")


def screen_point_to_xy_dor_camera(point):
    print(point)
    f_x = 519.39266469
    f_y = 518.33370266
    o_x = 321.4443738
    o_y = 243.68356671
    z = 200

    x = z * (point[0] - o_x) / f_x
    y = z * (point[1] - o_y) / f_y

    print(f"X: {x}, Y: {y}, Z: {z}")


def screen_point_to_xy_logi_camera(point):
    print(point)
    f_x = 626.0716671
    f_y = 622.99841535
    o_x = 297.73690509
    o_y = 249.3490038
    # z = 170

    x = z * (point[0] - o_x) / f_x
    y = z * (point[1] - o_y) / f_y

    print(f"X: {x}, Y: {y}, Z: {z}")

    return


if __name__ == "__main__":
    root = tk.Tk()
    CalibrationWindow(root, screen_point_to_xy_general)

    root.mainloop()
