import numpy as np


# disparities = 64  # num of disparities to consider
# distance_factor = 1.072  # divide by this in distance function, found from experiment


def disparity_to_depth(pixel_diff, focal_len, camera_dist):
    """
    find distance of an object from disparity
    :param pixel_diff: pixel difference between 2 cameras, using x coordinate
    :return: distance from camera plane
    """
    return (focal_len * camera_dist) / pixel_diff if pixel_diff > 0 else np.inf


def pixel_to_angles(pixels, focal_length, resolution):
    """
    :param pixels:
    :param focal_length:
    :param resolution:
    :return: theta,phi of pixel
    """
    theta = np.arctan((pixels[0] - resolution[0] / 2) / focal_length)
    phi = np.arctan((pixels[1] - resolution[1] / 2) / focal_length)
    return theta, phi


def angles_to_xyz(theta, phi, depth):
    return np.asarray([np.sin(theta), 1, np.sin(phi)]) * depth


def pixel_depth_to_xyz(pixels, depth, resolution, f):
    theta, phi = pixel_to_angles(pixels, f, resolution)
    return angles_to_xyz(theta, phi, depth)

