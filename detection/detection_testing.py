import threading
import tkinter as tk

import cv2
from PIL import Image, ImageTk

import numpy as np

from detection.detection_utils import detect_ball
from launch.main import mask

CAMERA_INDEX = 1
MASK = mask
BALL_SIZE = [15, 50]


class TestingWindow:

    def __init__(self, root, mask):
        self.root = root
        self.root.title("Camera Viewer and Control")
        self.root.geometry("800x600")

        # Create a label to display the camera image
        self.label = tk.Label(self.root)
        self.label.pack(padx=10, pady=10)

        self.mask = mask

        # Start reading camera feed in a separate thread
        self.camera_thread = threading.Thread(target=self.read_camera)
        self.camera_thread.daemon = True
        self.camera_thread.start()

    def read_camera(self):
        cap = cv2.VideoCapture(CAMERA_INDEX)  # Open the default camera (change index if you have multiple cameras)
        if not cap.isOpened():
            print("Error: Could not open camera.")
            return

        while cap.isOpened():
            ret, frame = cap.read()
            if not ret:
                print("Error: Failed to capture frame.")
                break

            # Convert frame from BGR to RGB and then to ImageTk format
            x, y, _ = detect_ball(frame, self.mask, min_ball_radius=BALL_SIZE[0], max_ball_radius=BALL_SIZE[1])
            # if x is not None and y is not None:
            #     cv2.circle(frame, (x, y), 4, (0, 0, 255), 2)

            frame_rgb = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
            img = Image.fromarray(frame_rgb)
            img_tk = ImageTk.PhotoImage(image=img)

            # Update GUI with the new image
            self.label.config(image=img_tk)
            self.label.image = img_tk

            # Check for arrow key press events
            key = cv2.waitKey(1) & 0xFF
            if key == 27:  # ESC key to exit
                break

        cap.release()
        cv2.destroyAllWindows()


if __name__ == '__main__':
    root = tk.Tk()

    # r, g, b = (100, 180), (0, 80), (0, 80)
    h, s, v = (170, 10), (100, 255), (100, 255)

    def mask(x):
        if h[0] > h[1]:
            m1 = cv2.inRange(x, np.asarray([h[0], s[0], v[0]]), np.asarray([181, s[1], v[1]]))
            m2 = cv2.inRange(x, np.asarray([0, s[0], v[0]]), np.asarray([h[1], s[1], v[1]]))
            return m1 | m2
        else:
            return cv2.inRange(x, np.asarray([h[0], s[0], v[0]]), np.asarray([h[1], s[1], v[1]]))

    calibrator = TestingWindow(root, mask)

    def loop_over_mask():
        global h, s, v
        while True:
            inarr = input('enter (h/s/v min max): ').split()
            c = inarr[0]
            range = (int(inarr[1]), int(inarr[2]))
            if c == 'h':
                h = range
            elif c == 's':
                s = range
            elif c == 'v':
                v = range

    calib_thread = threading.Thread(target=loop_over_mask)
    calib_thread.start()

    root.mainloop()
