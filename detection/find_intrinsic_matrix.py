import numpy as np
import cv2
import glob
import keyboard
from pathlib import Path
import time

use_existing_pictures = True
camera_index = 1
camera_name = "new_cam"
base_path = Path("calib_images")

# termination criteria
criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)

Ch_Dim = (4, 4)
# Sq_size = 34  # mm
# obj_3D = np.zeros((Ch_Dim[0] * Ch_Dim[1], 3), np.float32)
# index = 0
# for i in range(Ch_Dim[0]):
#     for j in range(Ch_Dim[1]):
#         obj_3D[index][0] = i * Sq_size
#         obj_3D[index][1] = j * Sq_size
#         index += 1
# #print(obj_3D)
# obj_points_3D = []  # 3d point in real world space
# img_points_2D = []  # 2d points in image plane.

# prepare object points, like (0,0,0), (1,0,0), (2,0,0) ....,(6,5,0)
objp = np.zeros((Ch_Dim[0] * Ch_Dim[1], 3), np.float32)
objp[:, :2] = np.mgrid[0:Ch_Dim[1], 0:Ch_Dim[0]].T.reshape(-1, 2)

# Arrays to store object points and image points from all the calib_images.
objpoints = []  # 3d point in real world space
imgpoints = []  # 2d points in image plane.

cap = cv2.VideoCapture(camera_index)
if not cap.isOpened():
    print("Error: Could not open one or both cameras.")
    quit()

if not base_path.exists():
    base_path.mkdir()
cam_img_path = base_path / Path(camera_name)
if not cam_img_path.exists():
    cam_img_path.mkdir()

if not use_existing_pictures:
    for i in range(20):
        t = time.time()
        while time.time() - t < 2:
            ret1, frame = cap.read()
            if ret1 is False:
                print("Error: Failed to capture frame.")
                break
            cv2.imshow('Camera Feeds', frame)
            cv2.waitKey(100)
        print("Taking picture")
        # while not keyboard.is_pressed('q'):
        #     ret1, frame = cap.read()
        #     cv.imshow('Camera Feeds', frame)
        #     cv.waitKey(5)
        # while not keyboard.is_pressed('w'):
        #     pass

        full_path = cam_img_path / Path(f"{i}.jpg")
        cv2.imwrite(str(full_path), frame)

images = glob.glob(f'{cam_img_path}/*.jpg')

print("Searching for chessboard corners...")
for fname in images:
    img = cv2.imread(fname)
    # cv.imshow('Camera Feeds', img)
    # cv.waitKey(10000)
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    # Find the chess board corners
    ret, corners = cv2.findChessboardCorners(gray, Ch_Dim, None)
    print(ret)
    # If found, add object points, image points (after refining them)
    if ret == True:
        objpoints.append(objp)
    else:
        print("No corners found")
        continue

    # corners2 = cv.cornerSubPix(gray, corners, (11, 11), (-1, -1), criteria)
    corners2 = corners
    imgpoints.append(corners2)

    # Draw and display the corners
    cv2.drawChessboardCorners(img, Ch_Dim, corners2, ret)
    cv2.imshow('img', img)
    cv2.waitKey(3000)

cv2.destroyAllWindows()

ret, mtx, dist, rvecs, tvecs = cv2.calibrateCamera(objpoints, imgpoints, gray.shape[::-1], None, None)

print("Camera Matrix:\n", mtx)

with open(cam_img_path / Path("calib_data.json"), "w") as f:
    f.write(f"{{\"camera_matrix\": {mtx.tolist()}, \"dist_coeff\": {dist.tolist()}}}")