import time

import cv2
import numpy as np
from matplotlib import pyplot as plt

from detection import depth_utils
from detection.detection_utils import detect_ball, RED_UPPER_COLOR, RED_LOWER_COLOR


def main():
    # Open a connection to the first and second cameras (indices 0 and 1)
    resolution = (640, 480)
    cap1 = cv2.VideoCapture(1)
    cap2 = cv2.VideoCapture(0)

    if not cap1.isOpened() or not cap2.isOpened():
        print("Error: Could not open one or both cameras.")
        return
    coords_2d = []
    dists = []
    for i in range(200):
        start = time.time()
        # Capture frame-by-frame from both cameras
        ret1, frame1 = cap1.read()
        ret2, frame2 = cap2.read()

        if not ret1 or not ret2:
            print("Error: Could not read frame from one or both cameras.")
            break
        # Resize frames to the same size (optional, depending on your camera setup)
        frame1 = cv2.resize(frame1, resolution)
        frame2 = cv2.resize(frame2, resolution)

        masks = [[np.asarray(RED_LOWER_COLOR), np.asarray(RED_UPPER_COLOR)],
                 [np.array([0, 120, 70]), np.array([5, 255, 255])]]
        # frame1, ball_pos1 = detect_ball_booly(frame1, color_ranges=masks, wrap_hue=True)  # you here bitch
        ball_pos1 = detect_ball(frame1, min_ball_radius=10)
        ball_pos2 = detect_ball(frame2, min_ball_radius=10)

        try:
            distance = depth_utils.disparity_to_depth(np.abs(ball_pos1[0] - ball_pos2[0]), 100)
            print(f"left: {ball_pos1}, right: {ball_pos2}")
            print(distance)
            coords_2d.append(ball_pos2[:2])
            dists.append(distance)
        except Exception:
            pass
        # Concatenate the frames horizontally
        combined_frame = np.hstack((frame1, frame2))

        # Display the resulting frame
        cv2.imshow('Camera Feeds', combined_frame)

        # Break the loop on 'q' key press
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

        finished_loop_time = time.time()
        # print(f"time taken is {(finished_loop_time - start):2f}")
    # When everything is done, release the captures

    cap1.release()
    cap2.release()
    cv2.destroyAllWindows()

    coords_2d = np.asarray(coords_2d)
    dists = np.asarray(dists)

    xyz = depth_utils.pixel_depth_to_xyz(coords_2d, dists, resolution=resolution)
    crop = [np.max(xyz), 0]
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.scatter(xyz[:, 0], xyz[:, 1], xyz[:, 2], label="before")
    ax.scatter(crop, crop, crop)
    plt.show()

    return xyz


def remove_background(image):
    # Load the image
    if image is None:
        print("Error: Image not found.")
        return

    # Convert the image to HSV color space
    hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)

    # Define range for red color in HSV
    lower_red1 = np.array([0, 120, 70])
    upper_red1 = np.array([10, 255, 255])
    lower_red2 = np.array([170, 120, 70])
    upper_red2 = np.array([180, 255, 255])

    # Create masks for red color
    mask1 = cv2.inRange(hsv, lower_red1, upper_red1)
    mask2 = cv2.inRange(hsv, lower_red2, upper_red2)
    red_mask = cv2.bitwise_or(mask1, mask2)

    # Invert the mask to get the background
    background_mask = cv2.bitwise_not(red_mask)

    # Convert the mask to 3 channels
    red_mask_3channel = cv2.cvtColor(red_mask, cv2.COLOR_GRAY2BGR)

    # Remove the background
    foreground = cv2.bitwise_and(image, red_mask_3channel)

    # Set the background to white
    background = np.full(image.shape, 255, dtype=np.uint8)
    background = cv2.bitwise_and(background, background, mask=background_mask)

    # Combine foreground and background
    result = cv2.add(foreground, background)
    cv2.imwrite("no_backgroung_ball.png", result)
    # cv2.imshow('Original Image', image)
    # cv2.imshow('Red Ball with Background Removed', result)
    # cv2.waitKey(0)
    # cv2.destroyAllWindows()
    return result


def save_vids(out_file, frames=100):
    cap1 = cv2.VideoCapture(1)
    frame_size = (640, 480)
    out1 = cv2.VideoWriter(out_file + ".MOV", 0x7634706d, fps=30, frameSize=frame_size)
    if not cap1.isOpened():
        print("Error: Could not open webcam.")
        return

    for _ in range(frames):
        ret1, frame1 = cap1.read()
        if ret1:
            out1.write(frame1)


if __name__ == "__main__":
    # main()
    # time.sleep(5)
    save_vids("test2_",frames=600)
