from typing import Tuple, Callable

import cv2
import numpy as np

RED_LOWER_COLOR = np.array([170, 120, 70])
RED_UPPER_COLOR = np.array([180, 255, 255])


def detect_ball(frame: np.ndarray, color_mask: Callable[[np.ndarray], np.ndarray], min_ball_radius=30,
                max_ball_radius=100, min_circularity=0,
                crop=None) -> Tuple:
    """
    Detect the best ball-shaped object in the frame, according to the color bounds, radius constraints and circularity
    :param frame: Image as np array
    :param min_ball_radius:
    :param max_ball_radius:
    :param min_circularity:
    :param crop: optional: (x_low,y_low,x_high,y_high). If given, crop the image before searching but return the same pixels as if not cropped
    :return: (x, y, radius) [pixels]
    """
    if crop:   # todo add crop back when done debug
        x_l, y_l, x_h, y_h = crop
        cropped = frame[y_l:y_h,x_l:x_h]
        x, y, r = detect_ball(cropped, color_mask, min_ball_radius, max_ball_radius, min_circularity,
                              None)
        if None in (x, y, r):
            return x, y, r
        return x + x_l, y + y_l, r

    if not frame.any():
        return None, None, None
    # Convert the frame to HSV color space
    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

    # Find contours in the mask
    contours, _ = cv2.findContours(color_mask(hsv), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    # Initialize ball position
    ball_position = None

    # Filter contours based on size
    filtered_contours = [cnt for cnt in contours if
                         min_ball_radius <= cv2.minEnclosingCircle(cnt)[1] <= max_ball_radius]

    # Find the contour with closest-to-round shape
    best_contour = None
    best_circularity = 0
    for cnt in filtered_contours:
        # Calculate contour area and perimeter
        area = cv2.contourArea(cnt)
        perimeter = cv2.arcLength(cnt, True)

        # Calculate circularity (4 * pi * area / perimeter^2)
        circularity = (4 * np.pi * area) / (perimeter ** 2)

        # Update best contour if circularity is higher than the threshold
        if circularity > min_circularity and circularity > best_circularity:
            best_contour = cnt
            best_circularity = circularity

    if best_contour is None:
        return None, None, None

    # Calculate the minimum enclosing circle for the best contour
    ((x, y), radius) = cv2.minEnclosingCircle(best_contour)

    # Only consider the contour if the radius is within the specified bounds
    if min_ball_radius <= radius <= max_ball_radius:
        ball_position = int(x), int(y), int(radius)
        # Draw the circle and centroid on the frame
        cv2.circle(frame, (int(x), int(y)), int(radius), (0, 255, 255), 2)
        cv2.circle(frame, (int(x), int(y)), 5, (0, 0, 255), -1)

    return ball_position



if __name__ == '__main__':
    # print(detect_ball_from_path_old(r"IMG_2260.MOV", 4, [0, 690, 0, 1279], True))
    detect_ball(np.uint8(np.ones((1280, 720, 3))))
