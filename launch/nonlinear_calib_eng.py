import json
from typing import Tuple
from scipy.interpolate import griddata

import numpy as np

CALIB_DATA_FILENAME = 'nonlinear_calib_results.json'


class NonlinearCalibrationEngine:

    def __init__(self, calib_path=None):
        filepath = f"{calib_path}/{CALIB_DATA_FILENAME}" if calib_path is not None else CALIB_DATA_FILENAME
        try:
            with open(filepath, 'r') as calib_file:
                calib_data = json.load(calib_file)
        except Exception as e:
            print(f'can\'t find file: {filepath}')
            return

        self.points = calib_data['pts']
        self.base_values = calib_data['base_values']
        self.pitch_values = calib_data['pitch_values']

        print(f'{len(self.points)}, {len(self.base_values)}, {len(self.pitch_values)}')

    def ij_to_base_pitch_diffs(self, i, j) -> Tuple[float, float]:
        """
        i,j are normalized x,y , z is normal z
        """
        b_val = griddata(self.points, self.base_values, ([i], [j]))[0]
        p_val = griddata(self.points, self.pitch_values, ([i], [j]))[0]

        return b_val, p_val
