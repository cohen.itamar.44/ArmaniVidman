import winsound
import time
from arduino_controller import ArduinoController

def ttest_shoot():
    a = ArduinoController("COM5")
    time.sleep(0.1)


    while True:
        a.load_gun()
        input("enter to shoot:")
        sleep_in_sec = 1
        print(f"calling for shoot with delay {sleep_in_sec} sec")
        a.shoot()

        # time.sleep(sleep_in_sec - 0.4)
        frequency = 2500  # Set Frequency To 2500 Hertz
        duration = 1000  # Set Duration To 1000 ms == 1 second
        winsound.Beep(frequency, duration)


if __name__ == '__main__':
    ttest_shoot()