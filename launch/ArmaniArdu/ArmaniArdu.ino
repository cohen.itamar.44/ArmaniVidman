#include <Servo.h>

/* MAIN METHODS:
 - listen for input from the computer (need a protocol for base angle, pitch angle, shooting)
 - control base and pitch angle
 - shooting
 */

#define BASE_SERVO_PIN 9
#define PITCH_SERVO_PIN 10
#define GUN_TRIGGER_PIN 8

#define BASE_ENCODER_PIN A0
#define PITCH_ENCODER_PIN A1

#define ANGLES_INTERVAL_MS 100

class Gun {
  int _shootPin;
  unsigned long _shootTime;
  bool _isTriggerDown = false;
public:
  void attach(int shootPin)
  {
    pinMode(shootPin, OUTPUT);
    _shootPin = shootPin;
  }

  void update() {
    unsigned long currMillis = millis();
    if (_isTriggerDown && currMillis - _shootTime >= 200) {
      _isTriggerDown = false;
      digitalWrite(_shootPin, LOW);
    }
  }

  void shoot() {
    _isTriggerDown = true;
    _shootTime = millis();
    digitalWrite(_shootPin, HIGH);
  }
};

/*class MyServo {
  int c
public:
  void update() {

  }


}*/

Servo baseServo;
Servo pitchServo;
Gun gun;

void handle_serial_input();
void send_angles_serial();

long lastTimeAngles;

void setup() {
  Serial.begin(9600);

  baseServo.attach(BASE_SERVO_PIN);
  baseServo.writeMicroseconds(1500); 

  pitc  hServo.attach(PITCH_SERVO_PIN);
  gun.attach(GUN_TRIGGER_PIN);

  Serial.println("Ready");
}

void loop() {
  
  gun.update();

  // Read serial input
  if (Serial.available() > 0) {
    char incomingByte = Serial.read();

    handle_serial_input(incomingByte);
  }

  long time = millis();
  if (time > lastTimeAngles + ANGLES_INTERVAL_MS) {
    send_angles_serial();
    lastTimeAngles = time;
  }

  delay(10);
}

void handle_serial_input(char incomingByte) {
  if (incomingByte == 'b') { // base angle
    while (Serial.available() == 0);
    int value = Serial.parseInt();
    baseServo.writeMicroseconds(value);
  } else if (incomingByte == 'p') { // pitch angle
    while (Serial.available() == 0);
    int value = Serial.parseInt();
    pitchServo.writeMicroseconds(value);
  } else if (incomingByte == 's') { // shoot
    gun.shoot();
  }
}

void send_angles_serial() {
  int base_reading = analogRead(BASE_ENCODER_PIN);
  int pitch_reading = analogRead(PITCH_ENCODER_PIN);

  byte buf[] = {'b', base_reading, 'p', pitch_reading, '\n'};
  Serial.write(buf, 5);
}