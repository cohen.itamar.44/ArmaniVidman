import time

import cv2
import numpy as np
from matplotlib import pyplot as plt, cm

from launch.arduino_controller import ArduinoController
from prediction.math_utils import running_average
from calibration.depth_two_cam_test import CAMERAS
from calibration.depth_two_cam_test import xyz_to_screen_point
from launch.calibration_engine import CalibrationEngine
from launch.detection_controller import DetectionController, Camera
from launch.prediction_engine import PendulumPrediction
from launch.sample_filter import SampleFilter, MAX_PIXEL_THRESHOLD
from prediction.shape_fits import theta_to_3d, find_fit_and_theta


KIULET_CAM_INDEX = 1

def get_pixel_const(frame):
    return (100, 100)


def run_akiva(get_pixel):
    ardu_cnt = ArduinoController()
    cap = cv2.VideoCapture(KIULET_CAM_INDEX)


    theta, phi = calib_eng.xyz_to_theta_phi(halt_pos)
    ardu_cnt.set_base_angle(theta_to_base(theta))
    ardu_cnt.set_pitch_angle(phi_to_pitch(phi))


def main():
    run_akiva(get_pixel_const)


if __name__ == '__main__':
    main()