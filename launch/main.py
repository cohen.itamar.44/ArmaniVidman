import threading
import time

import cv2
import numpy as np
from matplotlib import pyplot as plt, cm

import tkinter as tk
from launch.arduino_controller import ArduinoController
from launch.nonlinear_calib_eng import NonlinearCalibrationEngine
from prediction.math_utils import running_average, from_3d_to_2d_plane, centralize, theta_from_xy
from calibration.depth_two_cam_test import CAMERAS
from calibration.depth_two_cam_test import xyz_to_screen_point
from launch.detection_controller import DetectionController, Camera
from launch.prediction_engine import PendulumPrediction
from launch.sample_filter import SampleFilter, MAX_PIXEL_THRESHOLD
from prediction.shape_fits import theta_to_3d, find_fit_and_theta

ball_size = [10, 50]


def width_fov_to_focal(width, fov):
    return width / (2 * np.tan(fov / 2))


# focal length in pixels, calculated from width,fov
# use width in virtual camera not physical
CAMS_DIST = 30
L_CAM_INDEX = 1
R_CAM_INDEX = 0


def mask(img):
    h, s, v = (170, 10), (100, 255), (100, 255)
    m1 = cv2.inRange(img, np.asarray([h[0], s[0], v[0]]), np.asarray([181, s[1], v[1]]))
    m2 = cv2.inRange(img, np.asarray([0, s[0], v[0]]), np.asarray([h[1], s[1], v[1]]))
    return m1 | m2


def theta_to_base(theta):
    return np.rad2deg(theta - np.pi / 2)


def phi_to_pitch(phi):
    return np.rad2deg(np.pi / 2 - phi)


def curr_time(t_0):
    return time.time() - t_0


def debug_main():
    time_after = 4
    # ardu_cnt = ArduinoController()
    ardu_cnt = None

    lcam = Camera(L_CAM_INDEX, 'new_cam', mask, ball_size)
    # remove some part of the video
    for i in range(10):
        lcam.cap.read()
    # im not using the second camera
    rcam = Camera(R_CAM_INDEX, 'new_cam', mask, ball_size)
    detection_cnt = DetectionController([lcam, rcam], CAMS_DIST)

    pred_eng = PendulumPrediction()
    flag = False
    calib_eng = NonlinearCalibrationEngine()
    i = 0
    detection_cnt.capture_ball_xyz()
    t_0 = time.time()
    for _ in range(150):
        try:
            ball_xyz = detection_cnt.capture_ball_xyz()
        except:
            continue
        if ball_xyz is None:
            continue
        # pred_eng.input_sample(ball_xyz, i * 1000 / 30)
        pred_eng.input_sample(ball_xyz, curr_time(t_0))
        i += 1
        if flag or pred_eng.get_certainty() >= 0.9:
            if curr_time(t_0) > pred_eng.t_end + time_after:
                print("first peak:", pred_eng.times[pred_eng.first_peak])
                print("t end: ", pred_eng.t_end)
                print("first_root: ", pred_eng.times[pred_eng.first_root])
                print("zero offset: ", pred_eng.zero_offset)
                break
            flag = True
            halt_pos, end_t = pred_eng.predict_halting_pos_time()
            continue
            theta, phi = calib_eng.xyz_to_theta_phi(halt_pos)
            ardu_cnt.set_base_angle(theta_to_base(theta))
            ardu_cnt.set_pitch_angle(phi_to_pitch(phi))

            # if curr_time(t_0) + 3 * pred_eng.get_frame_processing_time() > end_t:

            time.sleep(2)
            ardu_cnt.shoot()
            print("shot!!")

    thetas = np.linspace(0, np.pi * 2, 1000)
    xyz_circ = theta_to_3d(pred_eng.a, pred_eng.b, pred_eng.center, pred_eng.r, thetas)
    plot_xyz(pred_eng.measurements, xyz_circ)
    _, _, _, _, thetas = find_fit_and_theta(np.asarray(pred_eng.measurements))
    ts = np.linspace(min(pred_eng.relevant_times()), pred_eng.t_end + 0.2, 1000)
    plt.plot(pred_eng.relevant_times(), pred_eng.get_accelaration() / .033, label="acceleration")
    plt.plot(pred_eng.times, thetas - pred_eng.zero_offset, label="theta")
    plt.plot(ts, pred_eng.prediction_func(ts), label="prediction")
    plt.legend()
    plt.show()


class OriginCentralizer:
    def __init__(self, root, callback):
        root.title("Camera Viewer and Control")
        root.geometry("800x600")

        root.bind('<KeyPress>', callback)


def main():
    ardu_cnt = ArduinoController()
    # ardu_cnt = None
    lcam = Camera(L_CAM_INDEX, 'new_cam', mask, ball_size)
    # lcam = Camera(vid_path, 'new_cam', mask, ball_size)

    rcam = Camera(R_CAM_INDEX, 'new_cam', mask, ball_size)

    origin_b_val, origin_p_val = 0, 0

    def callback(event):
        nonlocal origin_b_val, origin_p_val
        if event.keysym not in ['Up', 'Down', 'Left', 'Right', 'w', 's', 'a', 'd', 'r']:
            return

        if event.keysym in ['Up', 'w']:
            origin_p_val += 1 if event.keysym == 'Up' else 0.1
        elif event.keysym in ['Down', 's']:
            origin_p_val -= 1 if event.keysym == 'Down' else 0.1
        elif event.keysym in ['Left', 'a']:
            origin_b_val += 1 if event.keysym == 'Left' else 0.1
        elif event.keysym in ['Right', 'd']:
            origin_b_val -= 1 if event.keysym == 'Right' else 0.1
        origin_b_val, origin_p_val = round(origin_b_val, 1), round(origin_p_val, 1)

        ardu_cnt.set_base_angle(origin_b_val)
        ardu_cnt.set_pitch_angle(origin_p_val)

    root = tk.Tk()
    OriginCentralizer(root, callback)
    root.mainloop()

    detection_cnt = DetectionController([lcam, rcam], CAMS_DIST)

    pred_eng = PendulumPrediction()
    calib_eng = NonlinearCalibrationEngine("../calibration")
    shot_already = False
    input("Press enter to start")
    t_0 = time.time()
    count_after = 0
    for _ in range(150):
        try:
            ball_xyz = detection_cnt.capture_ball_xyz()
        except:
            continue
        if ball_xyz is None:
            continue
        pred_eng.input_sample(ball_xyz, curr_time(t_0))
        if shot_already or pred_eng.get_certainty() >= 0.9:
            if shot_already:
                count_after += 1
                if count_after == 50:
                    break
                continue
            shot_already = True
            # try:
            print("first peak:", pred_eng.times[pred_eng.first_peak])
            print("t end: ", pred_eng.t_end)
            print("first_root: ", pred_eng.times[pred_eng.first_root])
            print("zero offset: ", pred_eng.zero_offset)
            halt_pos, end_t = pred_eng.predict_halting_pos_time()

            print("halt pos: ", halt_pos)
            print("a,b: ", pred_eng.a, pred_eng.b)
            print("r,center: ", pred_eng.r, pred_eng.center)

            i, j = xyz_to_screen_point(halt_pos, CAMERAS["new_cam"])
            b_diff, p_diff = calib_eng.ij_to_base_pitch_diffs(i / 640, j / 480)
            print("b_diff: ", b_diff, ", p_diff: ", p_diff)

            ardu_cnt.set_base_angle(origin_b_val + b_diff)
            ardu_cnt.set_pitch_angle(origin_p_val + p_diff)

            delay = end_t - curr_time(t_0) - 0.1
            # timer = threading.Timer(delay, lambda: print(10*"\nSHHHOOOOOTTTT!!!!!!!"))
            # Start the timer
            # timer.start()
            print("delay: ", delay)

            ardu_cnt.load_gun()
            ardu_cnt.shoot(delay * 1000)

            # except Exception as e:
            #     print(e)

    thetas = np.linspace(0, np.pi * 2, 1000)
    xyz_circ = theta_to_3d(pred_eng.a, pred_eng.b, pred_eng.center, pred_eng.r, thetas)
    plot_xyz(pred_eng.measurements, np.asarray(xyz_circ))
    plot_xyz(pred_eng.measurements)
    plot_xyz(theta_to_3d(pred_eng.a, pred_eng.b, pred_eng.center, pred_eng.r, pred_eng.relevant_thetas()))
    thetas = calc_theta_from_3d(np.asarray(pred_eng.measurements), pred_eng.a, pred_eng.b, pred_eng.center)
    measures = np.asarray(pred_eng.measurements)
    x_m, y_m, z_m = measures[:, 0], measures[:, 1], measures[:, 2]
    x_end_real = np.interp(pred_eng.t_end, pred_eng.times, x_m)
    y_end_real = np.interp(pred_eng.t_end, pred_eng.times, y_m)
    z_end_real = np.interp(pred_eng.t_end, pred_eng.times, z_m)
    print("xyz end real: ", [x_end_real, y_end_real, z_end_real])
    print("xyz end predicted: ", halt_pos)
    plt.plot(pred_eng.times, thetas - pred_eng.zero_offset, label="real data")

    ts = np.linspace(min(pred_eng.relevant_times()), pred_eng.t_end + 0.2, 1000)
    plt.plot(ts, pred_eng.prediction_func(ts), label="prediction")
    plt.legend()
    plt.xlabel("time[s]")
    plt.ylabel("$\\theta(t)$")
    plt.show()


def calc_theta_from_3d(data_3d, a, b, center):
    samples_2d = from_3d_to_2d_plane(data_3d, a, b)

    centralized_2d = centralize(center, samples_2d)
    # find theta of each point
    return theta_from_xy(centralized_2d)


def depth_testing():
    ball_size_bounds = [10, 100]
    lcam = Camera(L_CAM_INDEX, "new_cam", mask, ball_size_bounds)
    rcam = Camera(R_CAM_INDEX, "new_cam", mask, ball_size_bounds)
    detec_cnt = DetectionController([lcam, rcam], CAMS_DIST)
    dists = []
    coords_2d = []
    xyzs = []
    for i in range(40):
        xyz = detec_cnt.capture_ball_xyz()
        ball_pos1 = lcam.last_pos
        ball_pos2 = rcam.last_pos

        frame1 = lcam.last_frame
        frame2 = rcam.last_frame
        if frame1 is None or frame2 is None:
            continue
        try:
            distance = xyz[1]
            print(f"left: {ball_pos1}, right: {ball_pos2}")
            print(distance)
            coords_2d.append(ball_pos1[:2])
            dists.append(distance)
            xyzs.append(xyz)
        except Exception:
            pass

        combined_frame = np.hstack((frame1, frame2))

        # Display the resulting frame
        cv2.imshow('Camera Feeds', combined_frame)
        # Break the loop on 'q' key press
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
    xyzs = np.asarray(xyzs)
    plot_xyz(xyzs)


def plot_xyz(xyz, xyz2=None):
    xyz = np.asarray(xyz)
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    cmap = cm.get_cmap('RdYlGn')
    # Normalize indices to [0, 1] for color mapping
    indices = np.arange(len(xyz))
    norm = plt.Normalize(indices[0], indices[-1])
    colors = cmap(norm(indices))
    ax.scatter(xyz[:, 0], xyz[:, 1], xyz[:, 2], c=colors, label="before")
    if xyz2 is not None:
        ax.scatter(xyz2[:, 0], xyz2[:, 1], xyz2[:, 2], c="blue")
    # n = [0]
    # ax.scatter(n, n, n)

    # ax.set_ylim3d(0, 400)
    # ax.set_zlim3d(0, 400)
    # ax.set_xlim3d(-200, 200)
    plt.show()


def detection_testing(cam_index):
    cam = Camera(cam_index, "new_cam", mask, ball_size)
    coords_2d = []
    for i in range(1000):
        t = time.time()
        ball_pos = cam.capture_xyr()
        frame = cam.last_frame
        if frame is None:
            continue
        if ball_pos:
            print(ball_pos)
            coords_2d.append(ball_pos[:2])

        # Display the resulting frame
        mask_3_channel = cv2.cvtColor(mask(frame), cv2.COLOR_GRAY2BGR)

        frames = np.hstack([frame, mask_3_channel])
        cv2.imshow('Camera Feeds', frames)
        # Break the loop on 'q' key press
        if cv2.waitKey(50) & 0xFF == ord('q'):
            break

    print(coords_2d)


if __name__ == '__main__':
    # arr = np.arange(1,10)
    # print(len(arr))
    # print(len(running_average(arr,2)))
    # vid_path = "../recordings/large_angles_220cm.MOV"
    vid_path = "../recordings/small_angles_220cm.MOV"
    # vid_path = 1
    main()
    # debug_main()
    # depth_testing()
    # cam_index = 0
    # detection_testing(L_CAM_INDEX)
    # detection_testing(R_CAM_INDEX)
