import threading
import time
from typing import Tuple

import numpy as np
import serial

PORT = 'COM10'
BAUDRATE = 9600
TIMEOUT = 1

# 0-255 RANGE VALUES FOR BASE AND PITCH SERVO, NEED TO BE FINE TUNED BEFORE START
BASE_90_DEG_RANGE = (500, 2500)  # -45 deg to 45 deg
PITCH_90_DEG_RANGE = (500, 2500)  # -45 deg to 45 deg


class ArduinoController:
    def __init__(self, port=PORT):
        print(f'Connecting to serial on port {port}...')
        self.serial = serial.Serial(port, BAUDRATE, timeout=TIMEOUT)
        print('Connected.')

        self._base_angle, self._pitch_angle = 0, 0

    def set_base_angle(self, angle: float):
        if abs(angle) > 45:
            raise Exception('Angle bigger than 45 degs for base, can\'t aim there')
        value = self.convert_base_angle_to_value(angle)
        self.serial.write(f'b{str(value)}\n'.encode())

    def set_pitch_angle(self, angle: float):
        if abs(angle) > 45:
            raise Exception('Angle bigger than 45 degs for pitch, can\'t aim there')
        value = self.convert_pitch_angle_to_value(angle)
        self.serial.write(f'p{str(value)}\n'.encode())

    def shoot(self, delay_ms=0):
        self.serial.write(f's{str(delay_ms)}\n'.encode())

    def load_gun(self):
        self.serial.write('l\n'.encode())

    def unload_gun(self):
        self.serial.write('u\n'.encode())

    @staticmethod
    def convert_base_angle_to_value(angle: float) -> int:
        return np.interp(angle, (-45, 45), BASE_90_DEG_RANGE)

    @staticmethod
    def convert_pitch_angle_to_value(angle: float) -> int:
        return np.interp(angle, (-45, 45), PITCH_90_DEG_RANGE)


if __name__ == '__main__':
    ardu_cnt = ArduinoController()
    time.sleep(1)
    ardu_cnt.load_gun()
    last_base = None
    last_pitch = None

    try:
        while True:
            cmd = input("Enter a command ('p' or 'b' + number from -45 to 45): ")

            if len(cmd) == 0:
                continue

            letter = cmd[0]
            number = 0

            if len(cmd) > 1:
                try:
                    number = float(cmd[1:])
                except Exception:
                    print('wrong format')
                    continue

            if letter == 'b':
                ardu_cnt.set_base_angle(number)
                last_base = number
                print(f'aiming base at {number}')
            elif letter == 'p':
                ardu_cnt.set_pitch_angle(number)
                last_pitch = number
                print(f'aiming pitch at {number}')
            elif letter == 's':
                ardu_cnt.shoot()
                print('shooting')
            elif letter == 'l':
                ardu_cnt.load_gun()
    except Exception as e:
        print(e)
    finally:
        if last_base is not None:
            step = 5
            step = -step if last_base < 0 else step
            while abs(last_base) > 0:
                last_base = 0 if last_base-step < 0 else last_base-step
                ardu_cnt.set_base_angle(last_base)
                time.sleep(0.5)
        if last_pitch is not None:
            step = 5
            step = -step if last_base < 0 else step
            while abs(last_pitch) > 0:
                last_pitch = 0 if last_pitch-step < 0 else last_pitch-step
                ardu_cnt.set_pitch_angle(last_pitch)
                time.sleep(0.5)