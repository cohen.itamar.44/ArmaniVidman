from typing import List

import numpy as np

norm_sqrd = lambda p1, p2: np.sum((p1 - p2) ** 2)

MAX_PIXEL_THRESHOLD = 50  # in pixels


class SampleFilter:

    def __init__(self, threshold):
        self.stable_value = None

        self.last_measure = None
        self.threshold_sqrd = MAX_PIXEL_THRESHOLD ** 2

    def check_sample(self, sample: np.ndarray) -> List[np.ndarray]:
        out_samples = []
        if self.last_measure is not None:

            if self.stable_value is None:  # hasn't had a stable value yet
                if self.are_two_last_samples_consistent(sample):  # first stable value, add both
                    self.stable_value = sample
                    out_samples.append(self.last_measure)
                    out_samples.append(sample)
            elif norm_sqrd(self.stable_value, sample) < self.threshold_sqrd \
                    or self.are_two_last_samples_consistent(sample):  # normal check, and then a check if the stable_value is wrong
                self.stable_value = sample
                out_samples.append(sample)

        self.last_measure = sample
        return out_samples

    def are_two_last_samples_consistent(self, sample):
        return norm_sqrd(self.last_measure, sample) < self.threshold_sqrd
