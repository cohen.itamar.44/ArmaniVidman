import json
from typing import Tuple
from scipy.interpolate import RegularGridInterpolator

import numpy as np

CALIB_DATA_FILENAME = 'calib_results.json'

class CalibrationEngine:

    def __init__(self, calib_path=None):
        """
        :param cam_to_gun - vector from camera to gun xyz
        """
        filepath = f"{calib_path}/{CALIB_DATA_FILENAME}" if calib_path is not None else CALIB_DATA_FILENAME
        with open(filepath, 'r') as calib_file:
            calib_data = json.load(calib_file)

        x_pt, y_pt, z_pt = calib_data['x_pt'], calib_data['y_pt'], calib_data['z_pt']
        z_min, z_max = calib_data['z_min'], calib_data['z_max']
        x_range, y_range = calib_data['offset']

        pts = [np.linspace(*x_range, x_pt), np.linspace(*y_range, y_pt), np.linspace(z_min, z_max, z_pt)]
        pitch_values = np.array(calib_data['pitch_angles'])
        base_values = np.array(calib_data['base_angles'])

        if z_pt == 1:
            z_pt = 3
            pts[2] = np.linspace(0, 1, z_pt)
            pitch_values = np.repeat(pitch_values, z_pt, axis=2)
            base_values = np.repeat(base_values, z_pt, axis=2)

        self.base_interpolator = RegularGridInterpolator(points=pts, values=base_values)
        self.pitch_interpolator = RegularGridInterpolator(points=pts, values=pitch_values)


    def ijz_to_base_pitch_angles(self, ijz: np.ndarray) -> Tuple[float, float]:
        """
        i,j are normalized x,y , z is normal z
        """
        print(ijz)
        b_val = self.base_interpolator(ijz)
        p_val = self.pitch_interpolator(ijz)

        return b_val, p_val
