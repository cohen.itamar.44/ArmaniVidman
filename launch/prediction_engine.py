from abc import ABC
from typing import Tuple

import numpy as np
from scipy.signal import find_peaks
from prediction import solver_1d, shape_fits, math_utils

frames_before_peak = 7


class PredictionEngine(ABC):

    def __init__(self):
        self.measurements = []
        self.times = []
        self.reset_state()

    def reset_state(self):
        self.measurements = []
        self.times = []

    def input_sample(self, pos, time):
        self.measurements.append(pos)
        self.times.append(time)

    def get_frame_processing_time(self):
        """
        :return: average time to process a single frame
        """
        times = self.times
        if len(times) < 2:
            return 100000
        return (times[-1] - times[0]) / (len(times) - 1)

    def get_certainty(self) -> float:
        """
        :return: value in [0,1]
        """
        raise NotImplementedError

    def predict_halting_pos_time(self) -> Tuple[np.array, float]:
        """
        :return: (x, y, z), t_stop
        """
        raise NotImplementedError


class StaticPrediction(PredictionEngine):
    def get_certainty(self) -> float:
        return len(self.measurements) / 10

    def predict_halting_pos_time(self) -> Tuple[np.array, float]:
        return np.mean(np.asarray(self.measurements), axis=0), self.times[-1] + 0.5


class PendulumPrediction(PredictionEngine):
    def __init__(self):
        super().__init__()
        self.zero_offset = 0
        self.first_root = -1
        self.first_peak = None
        self.poly_coefs = None
        self.t_end = None
        self.theta_end = None
        self.thetas = None
        # params for plane and circle fit
        self.a = 0
        self.b = 0
        self.center = 0
        self.r = 0
        self.prediction_func = None

    def input_sample(self, pos, time):
        super().input_sample(pos, time)

    def calculate_fit(self):
        # find best fit plane and circle, calculate thetas
        self.a, self.b, self.center, self.r, self.thetas = shape_fits.find_fit_and_theta(
            np.asarray(self.measurements))

    def get_certainty(self) -> float:
        if len(self.measurements) < 10:
            return 0
        # find the plane and circle the points are on
        self.calculate_fit()

        # find the first peak in the data
        self.first_peak = self.find_first_peak()

        if self.first_peak == -1:
            return 0

        # find the first root after the peak
        self.find_first_root()
        if self.first_root == -1:
            return 0

        # fit polynomial
        self.poly_coefs = solver_1d.fit_polynomial(self.relevant_times(), self.relevant_thetas())

        # calculate end point
        self.calculate_end()

        # calculate root from fit, if we are past it return 1 (should always happen)
        root_t = solver_1d.find_first_root(self.relevant_times()[0], self.poly_coefs)
        if max(self.times) > root_t :
            return 1
        return 0

    def predict_halting_pos_time(self) -> Tuple[np.array, float]:
        # convert back to 3d point and return
        print("theta end", self.theta_end)
        return shape_fits.theta_to_3d(self.a, self.b, self.center, self.r, np.asarray([self.theta_end]))[0], self.t_end

    def find_first_peak_from_z(self):
        z = np.asarray(self.measurements)[:, 2]
        peaks, _ = find_peaks(z)
        if len(peaks) > 0:
            return peaks[0]
        return -1

    def find_first_peak(self):
        peaks, _ = find_peaks(np.asarray(self.thetas))

        # Find all troughs (local minima) by finding peaks in the negative of the array
        troughs, _ = find_peaks(-np.asarray(self.thetas))

        if len(peaks) == 0 and len(troughs) == 0:
            return -1
        if len(peaks) == 0:
            return troughs[0]
        return peaks[0]

    def calculate_end(self):
        self.t_end, self.t_root, self.prediction_func = solver_1d.predict_end(self.relevant_times(),
                                                                              self.relevant_thetas())
        self.t_end -= .1
        self.theta_end = self.prediction_func(np.asarray((self.t_end,)))
        self.theta_end += self.zero_offset

    def relevant_times(self):
        return self.times[max(self.first_peak - frames_before_peak, 0):self.first_root]

    def relevant_thetas_not_adjusted(self):
        return self.thetas[max(self.first_peak - frames_before_peak, 0):self.first_root]

    def relevant_thetas(self):
        return self.relevant_thetas_not_adjusted() - self.zero_offset

    def get_accelaration(self):
        return math_utils.running_average(np.gradient(np.gradient(self.relevant_thetas_not_adjusted())), 2)

    def find_first_root_from_z(self):
        offset = max(0, self.first_peak - frames_before_peak)
        relevant_z = np.asarray(self.measurements[offset:])[:, 2]
        valleys, _ = find_peaks(-relevant_z)
        if len(valleys) > 0 :
            self.first_root = valleys[0] + offset
            return
        self.first_root = -1

    def find_first_root(self):
        offset = max(0, self.first_peak - frames_before_peak)
        relevant_thetas = self.thetas[offset:]
        start_sign = np.sign(relevant_thetas[0])
        try:
            self.first_root = np.where(start_sign != np.sign(relevant_thetas))[0][0] + offset
            self.zero_offset = relevant_thetas[np.argmin(np.abs(self.get_accelaration()))]
            self.zero_offset = 0
        except Exception:
            self.first_root = -1
