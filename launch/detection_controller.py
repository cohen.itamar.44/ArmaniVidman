from typing import Callable, Tuple

import cv2
import numpy as np

from calibration.depth_two_cam_test import screen_points_to_depth, screen_point_depth_to_xyz
from detection import depth_utils
from detection.detection_utils import detect_ball


class Camera:
    def __init__(self, index, name, color_mask: Callable[[np.ndarray], np.ndarray], ball_size_bounds):
        self.cap = cv2.VideoCapture(index)
        self.name = name
        self.last_pos = None
        self.last_frame = None
        self.color_mask = color_mask
        self.min_ball, self.max_ball = ball_size_bounds

        res = (int(self.cap.get(cv2.CAP_PROP_FRAME_WIDTH)), int(self.cap.get(cv2.CAP_PROP_FRAME_HEIGHT)))
        self.resolution = np.asarray(res)

    def _get_crop(self):
        if self.last_pos is None:
            return None
        x, y = self.last_pos
        dx, dy = self.resolution / 16
        return int(max(x - dx, 0)), int(max(y - dy, 0)), \
            int(min(x + dx, self.resolution[0])), int(min(y + dy, self.resolution[1]))

    def is_opened(self):
        return self.cap.isOpened()

    def capture_ball(self):
        """
        :return: (x,y) in pixels
        """
        res = self.capture_xyr()
        return None if res is None else res[:2]

    def capture_xyr(self):
        """
        take a picture and detect the ball
        :return: (x,y,r) in pixels
        """
        ret, frame = self.cap.read()
        self.last_frame = frame
        if not ret:
            return None

        crop = self._get_crop()
        x, y, r = detect_ball(frame, self.color_mask, min_ball_radius=self.min_ball, max_ball_radius=self.max_ball,
                              crop=crop)

        if None in (x, y, r):
            x, y, r = detect_ball(frame, self.color_mask, min_ball_radius=self.min_ball,
                                  max_ball_radius=self.max_ball )
        if None in (x, y, r):
            return None
        self.last_pos = x, y
        return x, y, r


class DetectionController:

    def __init__(self, cams, cams_distance):
        self.cams = cams
        self.distance = cams_distance
        for cam in self.cams:
            if not cam.is_opened():
                print("Error: Could not open one or both cameras.")
                return

    def capture_ball_xyz(self) -> np.array:
        poses = [cam.capture_ball() for cam in self.cams]
        # for pos in poses:
        #     if pos is None:
        #         return None

        # depth = screen_points_to_depth(*poses, self.cams[0].name, self.cams[1].name)
        # print(depth)
        depth = 300
        return screen_point_depth_to_xyz(poses[0], depth, self.cams[0].name)


def test_camera_index(i):
    cap = cv2.VideoCapture(i)
    for i in range(100):
        ret, frame = cap.read()
        if ret:
            cv2.imshow("", frame)
            cv2.waitKey(3)


if __name__ == '__main__':
    test_camera_index(1)
    test_camera_index(2)
